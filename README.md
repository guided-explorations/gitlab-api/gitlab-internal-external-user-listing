
# Internal External GitLab User Report

This project contains a Go script that fetches users' information from a GitLab group, classifies them as internal or external users, and generates an HTML report.

## Requirements

- Go (latest stable version)
- GitLab account and access to a group
- Personal access token from GitLab with `api` scope

## Script Description

The script uses GitLab's API to fetch all the users from a specific group, which is identified by its ID. The group ID and GitLab personal access token are passed as environment variables.

The script categorizes users as internal or external based on the `External` field of the user object. It then generates an HTML report that lists the usernames and email addresses of the internal and external users.

## Environment Variables

- `GITLAB_TOKEN`: Your GitLab personal access token. This is used to authenticate the API requests.
- `GROUP_ID`: The ID of the GitLab group from which you want to fetch user information.

## How to Run the Script

1. Set the environment variables `GITLAB_TOKEN` and `GROUP_ID` in your terminal session or your Go IDE.

   For a terminal session, use these commands:

   ```bash
   export GITLAB_TOKEN=your_access_token
   export GROUP_ID=your_group_id
   ```

   Replace `GITLAB_TOKEN` and `GROUP_ID` with your actual GitLab access token and group ID.

2. Run the script with the `go run` command:

   ```bash
   go run main.go
   ```

   This generates an HTML report named `users.html` in the same directory as the script.

## Output

The script generates an HTML file (`users.html`), which contains the list of internal and external users in the specified GitLab group. For each user, it shows the username and email address.