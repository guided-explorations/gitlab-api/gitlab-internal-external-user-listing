package main

import (
	"fmt"
	"html/template"
	"os"

	gitlab "github.com/xanzy/go-gitlab"
)

type UserData struct {
	Users []*gitlab.User
	Type  string
}

func main() {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		fmt.Println(err)
		return
	}

	groupID := os.Getenv("GROUP_ID") // Fetch group ID from environment variable

	options := &gitlab.ListGroupMembersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100, // Set to the number of users you expect to have (max is 100)
			Page:    1,
		},
	}

	internalUsers := make([]*gitlab.User, 0)
	externalUsers := make([]*gitlab.User, 0)

	// Fetch group members in batches
	for {
		members, resp, err := git.Groups.ListGroupMembers(groupID, options)
		if err != nil {
			fmt.Println(err)
			return
		}

		// Separate internal and external users
		for _, member := range members {
			user, _, err := git.Users.GetUser(member.ID, gitlab.GetUsersOptions{})
			if err != nil {
				fmt.Println(err)
				continue
			}

			if user.External {
				externalUsers = append(externalUsers, user)
			} else {
				internalUsers = append(internalUsers, user)
			}
		}

		// Exit the loop when we've fetched all pages
		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		// Move on to the next page
		options.Page = resp.NextPage
	}

	// Prepare data for HTML report
	data := []UserData{
		{Users: internalUsers, Type: "Internal"},
		{Users: externalUsers, Type: "External"},
	}

	// Define the HTML template
	tmpl := template.Must(template.New("report").Parse(`
		<!DOCTYPE html>
		<html>
		<head>
			<title>User Report</title>
		</head>
		<body>
			{{range .}}
				<h1>{{.Type}} Users</h1>
				<ul>
				{{range .Users}}
					<li>{{.Username}} ({{.Email}})</li>
				{{end}}
				</ul>
			{{end}}
		</body>
		</html>
	`))

	// Create the HTML file
	file, err := os.Create("users.html")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	// Execute the template and write the HTML to the file
	err = tmpl.Execute(file, data)
	if err != nil {
		fmt.Println(err)
	}
}

